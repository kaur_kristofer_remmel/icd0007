<?php


class Movie
{

    public $id;
    public $title;
    public $firstname;
    public $lastname;
    public $grade;
    public $isRead;
    public $authors1_id;

    public function __construct($id, $title, $firstname, $lastname, $grade, $isRead, $authors1_id)
    {
        $this->id = $id;
        $this->title = $title;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->authors1_id = $authors1_id;
    }


    public function __toString(){
        return $this->id  . $this->title . $this->grade . $this->isRead . $this->authors1_id;
    }


}