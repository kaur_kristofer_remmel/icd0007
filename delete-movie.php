<?php

require_once "functions.php";

$movieToDelete = $_POST["post-to-delete"];

deleteMovieByIndex($movieToDelete);

header("Location: index.php");
