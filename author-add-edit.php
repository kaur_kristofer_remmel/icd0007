<?php

require_once("functions.php");

$postIndex = $_GET["post-index"];

echo $postIndex;

$post = getAuthorByIndex($postIndex);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Lisa autor</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Filmid</a> |
    <a href="Movie-add.php" id="book-form-link">Lisa film</a> |
    <a href="authorsList.php" id="author-list-link">Režissöörid</a> |
    <a href="author-add.php" id="author-form-link">Lisa režissöör</a>
</nav>
<br>
<?php if (isset($_GET["errorMessage"])) : ?>
    <div class="error-message-blocks">
        <div class="error-block" id="error-block"><?=$_GET["errorMessage"]?></div>
    </div>
<?php endif; ?>
<form action="edit-author.php" method="post">
    <div class="normal-form">
        <div class="form-label-item"><label for="firstName">Eesnimi:</label></div>
        <div class="form-input-item"><input id="firstName" name="firstName"
                                            type="text" value="<?=$post["firstname"]?>"></div>

        <div class="form-label-item"><label for="lastName">Perekonnanimi:</label></div>
        <div class="form-input-item"><input id="lastName" name="lastName"
                                            type="text" value="<?=$post["lastname"]?>"></div>

        <div class="form-label-item"><label>Hinne:</label></div>
        <div class="form-input-item">
            <label>
                <input type="radio" <?php if (intval($post["grade"]) === 1) echo 'checked="checked"'?> name="grade" value="1">
            </label>1
            <label>
                <input type="radio" <?php if (intval($post["grade"]) === 2) echo 'checked="checked"'?> name="grade" value="2">
            </label>2
            <label>
                <input type="radio"  <?php if (intval($post["grade"]) === 3) echo 'checked="checked"'?>name="grade" value="3">
            </label>3
            <label>
                <input type="radio" <?php if (intval($post["grade"]) === 4) echo 'checked="checked"'?>name="grade" value="4">
            </label>4
            <label>
                <input type="radio" <?php if (intval($post["grade"]) === 5) echo 'checked="checked"'?>name="grade" value="5">
            </label>5
        </div>
        <input type="hidden" name="post-to-edit" value="<?=$post["id"]?>">

        <div class="Save-input">
            <input type="submit" name="submitButton" value="Salvesta">
        </div>
    </div>
</form>
<form action="delete-author.php" method="post">
    <div class="Save-input">
        <input type="hidden" name="post-to-delete" value="<?=$post["id"]?>"/>
        <input type="submit" name="deleteButton" value="Kustuta"/>
    </div>
</form>
<footer>
    <p>ICd0007 Näidisrakendus</p>
</footer>
</body>


</html>