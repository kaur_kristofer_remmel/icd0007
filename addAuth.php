<?php

require_once "functions.php";

$firstName = $_POST["firstName"];
$lastName = $_POST["lastName"];
$grade = $_POST["grade"];


$errorMessage = checkNames($firstName, $lastName);


if (strval($errorMessage) === ""){
    addAuthor($firstName, $lastName, $grade);
    $message = urlencode("Author was successfully added!");
    header("Location: authorsList.php?Message=".$message);
} else {
    echo $errorMessage;
    header("Location: author-add.php?errorMessage=".$errorMessage);
}




