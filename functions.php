<?php

require_once 'Author.php';
require_once 'Movie.php';


function getConnection() {

    $name = 'kaur_kristofer_suik';
    $pass = 'f273';
    $address = 'mysql:host=db.mkalmo.xyz;dbname=kaur_kristofer_suik';

    try {
        return new PDO($address, $name, $pass,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    }catch (PDOException $e) {
        throw new RuntimeException("cant connect");
    }
}



function getAuthors()
{
    $conn = getConnection();

    $stmt = $conn->prepare("select id, firstName, lastName, grade from authors1;");

    $stmt->execute();

    $authors = [];

    foreach ($stmt as $row){
        $authors[] = new Author(intval($row['id']), $row['firstName'], $row['lastName'], $row['grade']);


            /*["id" => $row['id'], "firstname" => $row['firstName'], "lastname" => $row['lastName']
            , "grade" => $row['grade']];*/
    }


    return $authors;
}


function addAuthor($firstName, $lastName, $grade)
{
    $conn = getConnection();

    $stmt = $conn->prepare("insert  into authors1 (firstName, lastname, grade) values (:f_name,:l_name,:grade);");

    $stmt->bindValue(':f_name', $firstName);
    $stmt->bindValue(':l_name', $lastName);
    $stmt->bindValue(':grade', $grade);

    $stmt->execute();
}


function deleteAuthorByIndex($indexOfdeleteItem)
{
    $connection = getConnection();

    $stmt = $connection->prepare("delete from authors1 where id = :deleteIndex;");

    $stmt->bindValue(':deleteIndex', $indexOfdeleteItem);

    $stmt->execute();
}


function getAuthorByIndex($authorIndex)
{
    $posts = getAuthors();

    foreach ($posts as $post) {

        if ($post->id == $authorIndex){
            return $post;
        }
    }
    return 'Author not found';
}


function editAuthor($firstName, $lastName, $grade, $postToEdit)
{
    $connect = getConnection();

    $stmt = $connect->prepare("UPDATE authors1 set firstName = :f_name, lastname = :l_name, grade = :grade where id = :postToEdit;");

    $stmt->bindValue(':f_name', $firstName);
    $stmt->bindValue(':l_name', $lastName);
    $stmt->bindValue(':grade', $grade);
    $stmt->bindValue(':postToEdit', $postToEdit);

    $stmt->execute();
}



function  checkNames($firstname, $lastname){
    $errorMessage = "";


    if (!((strlen($firstname) >= 1) && (strlen($firstname) <= 21))){
        $errorMessage = $errorMessage . "Firstname must have 1 to 21 characters!";
    }

    if (!((strlen($lastname) >= 2) && (strlen($lastname) <= 22))){
        $errorMessage = $errorMessage . " Lastname must have 2 to 22 characters!";
    }

    return $errorMessage;

}













function getMovieByIndex($itemIndex)
{
    $postss = getMoviePosts();

    foreach ($postss as $post) {
        if ($post->id == $itemIndex){
            return $post;
        }
    }
    return null;
}


function getMoviePosts()
{
    $connection = getConnection();



    $stmt = $connection->prepare("select movies.id, movies.title, movies.grade, movies.isRead
    , movies.authors1_id, a.firstName, a.lastName from movies
    left join authors1 a on movies.authors1_id = a.id;");

    $stmt->execute();

    $movies = [];

    foreach ($stmt as $row) {
        $movies[] = new Movie($row["id"], $row["title"], $row["firstName"], $row["lastName"],  $row["grade"], $row["isRead"], $row["authors1_id"]);


            /*["movies.id" => $row["id"],"title" => $row["title"],"a_firstname" => $row["firstName"],"a_lastname" => $row["lastName"]
            ,"grade" => $row["grade"],"isRead" => $row["isRead"], "authors.id" => $row["authors1_id"]];*/

    }

    return $movies;
}


function addBook($title, $authorIndex, $grade, $isRead)
{
    $connection = getConnection();

    $stmt = $connection->prepare("INSERT INTO movies (authors1_id, title, grade, isRead) values (:author_1Id, :title, :grade, :isRead);");

    $stmt->bindValue(':author_1Id',  $authorIndex);
    $stmt->bindValue(':title',  $title);
    $stmt->bindValue(':grade',  $grade);
    $stmt->bindValue(':isRead',  $isRead);

    $stmt->execute();
}


function deleteMovieByIndex($indexOfDeleteItem)
{
    $connection = getConnection();

    $stmt = $connection->prepare("delete from movies where id = :id;");

    $stmt->bindValue(":id", $indexOfDeleteItem);

    $stmt->execute();
}

function editMovie($title, $authorIndex, $grade, $isRead, $postToEdit)
{
    $connection = getConnection();

    $stmt = $connection->prepare("UPDATE movies set title = :title, authors1_id = :authorId, grade = :grade,
                                                                                isRead = :isRead where movies.id = :movieToEdit;");

    $stmt->bindValue("title", $title);
    $stmt->bindValue(":authorId", $authorIndex);
    $stmt->bindValue(":grade", $grade);
    $stmt->bindValue(":isRead", $isRead);
    $stmt->bindValue(":movieToEdit", $postToEdit);

    $stmt->execute();
}


function checkTitle($title){
    if (!((strlen($title) >= 3) && (strlen($title) <= 23))){
        return "Title must have 3 to 23 characters!";
    }
    return "";

}

function checkIfRead($isRead){
    if ($isRead == 'on'){
        return 1;
    }
    return 0;
}





