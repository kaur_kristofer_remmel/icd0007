<?php
require_once("functions.php");

$posts = getAuthors();

$error = false;

if ($_SERVER["REQUEST_METHOD"] === "POST"){

    echo $title = $_POST["title"];
    echo $author_1 = $_POST["author1"]; //hello
    echo $grade = $_POST["grade"];
    echo $isRead = $_POST["isRead"];

    $ifRead = checkIfRead($isRead);

    $errormessage = checkTitle($title);


    if (strval($errormessage) === "") {
        addBook($title, $author_1, $grade, $ifRead);
        $message = urlencode("Book was successfully added!");
        header("Location: index.php?Message=" . $message);
    } else {
        $error = true;
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Lisa Film</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Filmid</a> |
    <a href="Add-Movie.php" id="book-form-link">Lisa film</a> |
    <a href="List-Authors.php" id="author-list-link">Režissöörid</a> |
    <a href="Add-Author.php" id="author-form-link">Lisa režissöör</a>
</nav>
<br>
<?php if ($error) : ?>
    <div class="error-message-blocks">
        <div class="error-block" id="error-block"><?=$errormessage?></div>
    </div>
<?php endif; ?>
<form action="Add-Movie.php" method="post">
    <div class="normal-form">
        <div class="form-label-item"><label for="fn">Pealkiri:</label></div>
        <div class="form-input-item"><input id="fn" name="title" type="text"></div>

        <div class="form-label-item"><label for="fo">Autor :</label></div>
        <div class="form-input-item"><select id="fo" name="author1">
                <option value=0></option>
                <?php foreach ($posts as $ppost): ?>
                    <option value=<?=$ppost->id?>><?=$ppost->firstname . " " . $ppost->lastname?></option>
                <?php endforeach; ?>
            </select></div>

        <div class="form-label-item"><label>Hinne:</label></div>
        <div class="form-input-item">
            <label>
                <input type="radio" name="grade" value="1">
            </label>1
            <label>
                <input type="radio" name="grade" value="2">
            </label>2
            <label>
                <input type="radio" name="grade" value="3">
            </label>3
            <label>
                <input type="radio" name="grade" value="4">
            </label>4
            <label>
                <input type="radio" name="grade" value="5">
            </label>5
        </div>

        <div class="form-label-item"><label for="03">Vaadatud:</label></div>
        <div class="form-input-item"><input id="03" name="isRead" type="checkbox"></div>

        <div class="Save-input">
            <input type="submit" name='submitButton' value="Salvesta">
        </div>
    </div>
</form>
<footer>
    <p>ICd0007 Näidisrakendus</p>
</footer>
</body>
</html>