<?php


class MovieDao
{

    public function __construct()
    {
    }


    function getMoviePosts()
    {
        $connection = $this->getConnection();



        $stmt = $connection->prepare("select movies.id, movies.title, movies.grade, movies.isRead
    , movies.authors1_id, a.firstName, a.lastName from movies
    left join authors1 a on movies.authors1_id = a.id;");

        $stmt->execute();

        $movies = [];

        foreach ($stmt as $row) {
            $movies[] = new Movie($row["id"], $row["title"], $row["firstName"], $row["lastName"],  $row["grade"], $row["isRead"], $row["authors1_id"]);

        }

        return $movies;
    }


    function getConnection() {

        $name = 'kaur_kristofer_suik';
        $pass = 'f273';
        $address = 'mysql:host=db.mkalmo.xyz;dbname=kaur_kristofer_suik';

        try {
            return new PDO($address, $name, $pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }catch (PDOException $e) {
            throw new RuntimeException("cant connect");
        }
    }

    function getMovieByIndex($itemIndex)
    {
        $postss = $this->getMoviePosts();

        foreach ($postss as $post) {
            if ($post->id == $itemIndex){
                return $post;
            }
        }
        return null;
    }

    function addBook($movie)
    {
        $connection = $this->getConnection();

        $stmt = $connection->prepare("INSERT INTO movies (authors1_id, title, grade, isRead) values (:author_1Id, :title, :grade, :isRead);");

        $stmt->bindValue(':author_1Id',  $movie->authors1_id);
        $stmt->bindValue(':title',  $movie->title);
        $stmt->bindValue(':grade',  $movie->grade);
        $stmt->bindValue(':isRead',  $this->checkIfRead($movie->isRead));

        $stmt->execute();
    }


    function deleteMovieByIndex($indexOfDeleteItem)
    {
        $connection = $this->getConnection();

        $stmt = $connection->prepare("delete from movies where id = :id;");

        $stmt->bindValue(":id", $indexOfDeleteItem);

        $stmt->execute();
    }

    function editMovie($changedMovie)
    {
        $connection = $this->getConnection();

        $stmt = $connection->prepare("UPDATE movies set title = :title, authors1_id = :authorId, grade = :grade,
                                                                                isRead = :isRead where movies.id = :movieToEdit;");

        $stmt->bindValue("title", $changedMovie->title);
        $stmt->bindValue(":authorId", $changedMovie->authors1_id);
        $stmt->bindValue(":grade", $changedMovie->grade);
        $stmt->bindValue(":isRead", $this->checkIfRead($changedMovie->isRead));
        $stmt->bindValue(":movieToEdit", $changedMovie->id);

        $stmt->execute();
    }

    function checkIfRead($isRead){
        if ($isRead == 'on'){
            return true;
        }
        return null;
    }


}