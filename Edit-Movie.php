<?php

require_once ("functions.php");

$postIndex = $_GET["movieIndex"];

$moviePost = getMovieByIndex($postIndex);

$posts = getAuthors();

$error = false;

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    if ($_POST["post-to-edit"] === null){

        $movieToDelete = $_POST["post-to-delete"];

        deleteMovieByIndex($movieToDelete);

        header("Location: index.php");

    }else {

        $title = $_POST["title"];
        $author_1 = $_POST["author1"];
        $grade = $_POST["grade"];
        $isRead = $_POST["isRead"];
        $movieToEdit = $_POST["post-to-edit"];


        $errormessage = checkTitle($title);

        $ifRead = checkIfRead($isRead);

        if (strval($errormessage) === ""){
            editMovie($title, $author_1, $grade, $ifRead, $movieToEdit);
            $message = urlencode("Book was successfully updated!");
            header("Location: index.php?Message=".$message);
        } else {
            $error = true;
        }

    }
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Lisa Film</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Filmid</a> |
    <a href="Add-Movie.php" id="book-form-link">Lisa film</a> |
    <a href="List-Authors.php" id="author-list-link">Režissöörid</a> |
    <a href="Add-Author.php" id="author-form-link">Lisa režissöör</a>
</nav>
<br>
<?php if ($error) : ?>
    <div class="error-message-blocks">
        <div class="error-block" id="error-block"><?=$errormessage?></div>
    </div>
<?php endif; ?>
<form action="Edit-Movie.php" method="post">
    <div class="normal-form">
        <div class="form-label-item"><label for="fn">Pealkiri:</label></div>
        <div class="form-input-item"><input id="fn" name="title" type="text" value="<?=$moviePost->title?>"></div>

        <div class="form-label-item"><label for="fo">Autor 1:</label></div>
        <div class="form-input-item"><select id="fo" name="author1">
                <option name="author1" value=<?=$moviePost->authors1_id?>><?=$moviePost->firstname . " " . $moviePost->lastname?></option>
                <?php foreach ($posts as $ppost): ?>
                    <option name="author1" value=<?=$ppost->id?>><?=$ppost->firstname . " " . $ppost->lastname?></option>
                <?php endforeach; ?>
            </select></div>


        <div class="form-label-item"><label>Hinne:</label></div>
        <div class="form-input-item">
            <label>
                <input type="radio" <?php if (intval($moviePost->grade) === 1) echo 'checked="checked"'?> name="grade" value="1">
            </label>1
            <label>
                <input type="radio" <?php if (intval($moviePost->grade) === 2) echo 'checked="checked"'?> name="grade" value="2">
            </label>2
            <label>
                <input type="radio" <?php if (intval($moviePost->grade) === 3) echo 'checked="checked"'?> name="grade" value="3">
            </label>3
            <label>
                <input type="radio" <?php if (intval($moviePost->grade) === 4) echo 'checked="checked"'?> name="grade" value="4">
            </label>4
            <label>
                <input type="radio" <?php if (intval($moviePost->grade) === 5) echo 'checked="checked"'?> name="grade" value="5">
            </label>5
        </div>

        <div class="form-label-item"><label for="03">Vaadatud:</label></div>
        <div class="form-input-item"><input id="03" <?php if (isset($moviePost->isRead))
                echo 'checked="checked"'?>name="isRead" type="checkbox"></div>

        <input type="hidden" name="post-to-edit" value="<?=$moviePost->id?>">

        <div class="Save-input">
            <input type="submit" name="submitButton" value="Salvesta">
        </div>
    </div>
</form>
<form action="Edit-Movie.php" method="post">
    <div class="Save-input">
        <input type="hidden" name="post-to-delete" value="<?=$moviePost->id?>"/>
        <input type="submit" name="deleteButton" value="Kustuta"/>
    </div>
</form>
<footer>
    <p>ICd0007 Näidisrakendus</p>
</footer>
</body>
</html>