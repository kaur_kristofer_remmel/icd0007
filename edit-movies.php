<?php

require_once("functions.php");

echo $title = $_POST["title"];
echo $author_1 = $_POST["author1"];
$grade = $_POST["grade"];
$isRead = $_POST["isRead"];
$movieToEdit = $_POST["post-to-edit"]; //jjj


$errormessage = checkTitle($title);

$ifRead = checkIfRead($isRead);

if (strval($errormessage) === ""){
    editMovie($title, $author_1, $grade, $ifRead, $movieToEdit);
    $message = urlencode("Book was successfully updated!");
    header("Location: index.php?Message=".$message);
} else {
    header("Location: Movie-Add-Edit.php?errorMessage=".$errormessage);
}