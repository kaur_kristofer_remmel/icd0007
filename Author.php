<?php


class Author
{
    public $id;
    public $firstname;
    public $lastname;
    public $grade;

    public function __construct($id, $firstname, $lastname, $grade)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->grade = $grade;
    }


    public function __toString(){
        return $this->id . $this->firstname . $this->lastname . $this->grade;
    }


}