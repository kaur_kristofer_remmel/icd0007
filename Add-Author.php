<?php

require_once "functions.php";

$error = false;

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $firstName = $_POST["firstName"];
    $lastName = $_POST["lastName"];
    $grade = $_POST["grade"];

    $errorMessage = checkNames($firstName, $lastName);

    if (strval($errorMessage) === "") {
        addAuthor($firstName, $lastName, $grade);
        $message = urlencode("Author was successfully added!");
        header("Location: List-Authors.php?Message=" . $message);
    } else {
        $error = true;
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Lisa autor</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Filmid</a> |
    <a href="Add-Movie.php" id="book-form-link">Lisa film</a> |
    <a href="List-Authors.php" id="author-list-link">Režissöörid</a> |
    <a href="Add-Author.php" id="author-form-link">Lisa režissöör</a>
</nav>
<br>
<?php if ($error) : ?>
    <div class="error-message-blocks">
        <div class="error-block" id="error-block"><?=$errorMessage?></div>
    </div>
<?php endif; ?>
<br>


<form action="Add-Author.php" method="post">
    <div class="normal-form">
        <div class="form-label-item"><label for="firstName">Eesnimi:</label></div>
        <div class="form-input-item"><input id="firstName" name="firstName" type="text"></div>

        <div class="form-label-item"><label for="lastName">Perekonnanimi:</label></div>
        <div class="form-input-item"><input id="lastName" name="lastName" type="text"></div>

        <div class="form-label-item"><label>Hinne:</label></div>
        <div class="form-input-item">
            <label>
                <input type="radio" name="grade" value="1">
            </label>1
            <label>
                <input type="radio" name="grade" value="2">
            </label>2
            <label>
                <input type="radio" name="grade" value="3">
            </label>3
            <label>
                <input type="radio" name="grade" value="4">
            </label>4
            <label>
                <input type="radio" name="grade" value="5">
            </label>5
        </div>

        <div class="Save-input">
            <input type="submit" name="submitButton" value="Salvesta">
        </div>
    </div>
</form>
<footer>
    <p>ICd0007 Näidisrakendus</p>
</footer>
</body>


</html>