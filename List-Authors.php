<?php
require_once("functions.php");

$posts = getAuthors();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Autorid</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<nav>
    <a href="index.php" id="book-list-link">Filmid</a> |
    <a href="Add-Movie.php" id="book-form-link">Lisa film</a> |
    <a href="List-Authors.php" id="author-list-link">Režissöörid</a> |
    <a href="Add-Author.php" id="author-form-link">Lisa režissöör</a>
</nav>
<br>
<?php if (isset($_GET["Message"])) : ?>
<div class="error-message-blocks">
    <div class="message-block" id="message-block"><?=$_GET["Message"]?></div>
</div>
<?php endif; ?>
<br>
<div class="author-list">
    <div class="author-name-three-firstones">Eesnimi</div>
    <div class="author-name-three-firstones">Perekonnanimi</div>
    <div class="author-name-three-firstones grade-item">Hinne</div>
</div>

<hr>

<?php
foreach ($posts as $ppost): ?>
<div class="author-list">
    <div class="author-name"><a href="Edit-Author.php?post-index=<?=$ppost->id?>"><?=$ppost->firstname ?></a></div>
    <div class="author-name"><?=$ppost->lastname ?></div>
    <div class="grade-item"><?=$ppost->grade?></div>
    <br>
</div>
<?php endforeach; ?>

<footer>
    <p>ICd0007 Näidisrakendus</p>
</footer>
</body>

</html>
