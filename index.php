<?php
require_once('vendor/tpl.php');
require_once('MovieDao.php');
require_once('AuthorDao.php');
require_once('Request.php');
require_once('Movie.php');
require_once('Author.php');


$request = new Request($_REQUEST);

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'show_movie_list';


if ($cmd === 'show_movie_list'){
    $movies = new MovieDao();

    $data = [
        'messages' => $request->param('messages'),
        'movies' => $movies->getMoviePosts(),
        'path' => 'Movies-List.html'
    ];
    print renderTemplate('tpl/main.html', $data);

}


else if ($cmd === 'add_movie'){
    $authors = new AuthorDao();

    $data = [
        'authors' => $authors->getAuthors(),
        'path' => 'Add-Movie.html',
        'cmd' => 'save_movie'
    ];
    print renderTemplate('tpl/main.html', $data);
}

else if ($cmd === 'save_movie'){
    $movie = new Movie(null, $request->param('title'), $request->param('firstName'),$request->param('lastName'),
        $request->param('grade'),$request->param('isRead'), $request->param('author1'));
    $errors = checkTitle($movie);


    if (empty($errors)){
        $movies = new MovieDao();
        $movies->addBook($movie);
        header('Location: index.php?cmd=show_movie_list&messages=Added!');
    } else {
        $authors = new AuthorDao();

        $data = [
            'cmd' => 'save_movie',
            'authors' => $authors->getAuthors(),
            'messages' =>$errors,
            'movie' => $movie,
            'path' => 'Add-Movie.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    }
} else if ($cmd === 'edit_movie'){
    $movies = new MovieDao();
    $bool = false;

    if ($request->param('deleteButton')){
        $movies->deleteMovieByIndex($request->param('post-to-delete'));
        $data = [
            'messages' => 'Deleted!',
            'movies' => $movies->getMoviePosts(),
            'path' => 'Movies-List.html'
        ];
        print renderTemplate('tpl/main.html', $data);

    } else if ($request->param('title') || $request->param('grade') || $request->param('isRead')){
        $changedMovie = new Movie($request->param('movie_id'), $request->param('title'), $request->param('firstName'),$request->param('lastName'),
            $request->param('grade'),$request->param('isRead'), $request->param('author1'));
        $errors = checkTitle($changedMovie);

        if (empty($errors)){
            $movies->editMovie($changedMovie);
            header('Location: index.php?cmd=show_movie_list&messages=Updated!');
        } else {
            $authors = new AuthorDao();

            $data = [
                'cmd' => 'save_movie',
                'authors' => $authors->getAuthors(),
                'messages' =>$errors,
                'movie' => $changedMovie,
                'path' => 'Add-Movie.html'
            ];
            print renderTemplate('tpl/main.html', $data);
        }

    } else {
        $movie = $movies->getMovieByIndex($request->param('movieIndex'));
        $authors = new AuthorDao();

        $data = [
            'cmd' => 'edit_movie',
            'edit_mode' => true,
            'authors' => $authors->getAuthors(),
            'movie' => $movie,
            'path' => 'Add-Movie.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    }
}







else if ($cmd === 'show_author_list'){
    $authors = new AuthorDao();

    $data = [
        'messages' => $request->param('messages'),
        'authors' => $authors->getAuthors(),
        'path' => 'Authors-List.html'
    ];
    print renderTemplate('tpl/main.html', $data);
}


else if ($cmd === 'add_author'){

    $data = [
        'path' => 'Add-Author.html',
        'cmd' => 'save_author'
    ];
    print renderTemplate('tpl/main.html', $data);
}


else if ($cmd === 'save_author'){

    $author = new Author(0, $request->param('firstName'), $request->param('lastName'), $request->param('grade'));
    $errors = checkNames($author);


    if (empty($errors)){
        $authors = new AuthorDao();
        $authors->addAuthor($author);
        header('Location: index.php?cmd=show_author_list&messages=Added!');
    } else {
        $data = [
            'cmd' => 'save_author',
            'author' => $author,
            'messages' => $errors,
            'path' => 'Add-Author.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    }
}

else if ($cmd === 'edit_author'){
    $authorDao = new AuthorDao();

    if ($request->param('deleteButton')){
        $authorDao->deleteAuthorByIndex($request->param('post-to-delete'));

        $data = [
            'messages' => 'Deleted!',
            'authors' => $authorDao->getAuthors(),
            'path' => 'Authors-List.html'
        ];
        print renderTemplate('tpl/main.html', $data);

    } else if ($request->param('firstName') || $request->param('lastName') || $request->param('grade')){
        $changedAuthor = new Author($request->param('author_id'), $request->param('firstName'), $request->param('lastName'), $request->param('grade'));
        $errors = checkNames($changedAuthor);

        if (empty($errors)){
            $authorDao->editAuthor($changedAuthor);
            header('Location: index.php?cmd=show_author_list&messages=Updated!');
        } else {
            $data = [
                'edit_mode' => true,
                'cmd' => 'edit_author',
                'author' => $changedAuthor,
                'messages' => $errors,
                'path' => 'Add-Author.html'
            ];
            print renderTemplate('tpl/main.html', $data);
        }
    } else {
        $author = $authorDao->getAuthorByIndex($request->param('post-index'));

        $data = [
            'edit_mode' => true,
            'cmd' => 'edit_author',
            'author' => $author,
            'path' => 'Add-Author.html'
        ];
        print renderTemplate('tpl/main.html', $data);
    }
}


function  checkNames($author){
    $firstname = $author->firstname;
    $lastname = $author->lastname;
    $errorMessage = "";


    if (!((strlen(trim($firstname)) >= 1) && (strlen(trim($firstname)) <= 21))){
        $errorMessage = $errorMessage . "Firstname must have 1 to 21 characters!";
    }

    if (!((strlen(trim($lastname)) >= 2) && (strlen(trim($lastname)) <= 22))){
        $errorMessage = $errorMessage . " Lastname must have 2 to 22 characters!";
    }

    return $errorMessage;
}

function checkTitle($changedMovie){
    if (!((strlen(trim($changedMovie->title)) >= 3) && (strlen(trim($changedMovie->title)) <= 23))){
        return "Title must have 3 to 23 characters!";
    }
    return "";
}




