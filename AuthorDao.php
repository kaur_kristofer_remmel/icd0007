<?php


class AuthorDao
{
    public function __construct()
    {
    }

    public function getAuthors()
    {
        $conn = $this->getConnection();

        $stmt = $conn->prepare("select id, firstName, lastName, grade from authors1;");

        $stmt->execute();

        $authors = [];

        foreach ($stmt as $row){
            $authors[] = new Author(intval($row['id']), $row['firstName'], $row['lastName'], $row['grade']);

        }


        return $authors;
    }


    private function getConnection() {

        $name = 'kaur_kristofer_suik';
        $pass = 'f273';
        $address = 'mysql:host=db.mkalmo.xyz;dbname=kaur_kristofer_suik';

        try {
            return new PDO($address, $name, $pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }catch (PDOException $e) {
            throw new RuntimeException("cant connect");
        }
    }

    function addAuthor($author)
    {
        $conn = $this->getConnection();

        $stmt = $conn->prepare("insert  into authors1 (firstName, lastname, grade) values (:f_name,:l_name,:grade);");

        $stmt->bindValue(':f_name', $author->firstname);
        $stmt->bindValue(':l_name', $author->lastname);
        $stmt->bindValue(':grade', $author->grade);

        $stmt->execute();
    }


    function deleteAuthorByIndex($indexOfdeleteItem)
    {
        $connection = $this->getConnection();

        $stmt = $connection->prepare("delete from authors1 where id = :deleteIndex;");

        $stmt->bindValue(':deleteIndex', $indexOfdeleteItem);

        $stmt->execute();
    }


    function getAuthorByIndex($authorIndex)
    {
        $posts = $this->getAuthors();

        foreach ($posts as $post) {

            if ($post->id == $authorIndex){
                return $post;
            }
        }
        return 'Author not found';
    }


    function editAuthor($changedAuthor)
    {
        $connect = $this->getConnection();

        $stmt = $connect->prepare("UPDATE authors1 set firstName = :f_name, lastname = :l_name, grade = :grade where id = :postToEdit;");

        $stmt->bindValue(':f_name', $changedAuthor->firstname);
        $stmt->bindValue(':l_name', $changedAuthor->lastname);
        $stmt->bindValue(':grade', $changedAuthor->grade);
        $stmt->bindValue(':postToEdit', $changedAuthor->id);

        $stmt->execute();
    }


}